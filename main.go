package main

import (
	"cv_website/assets/database"
	"cv_website/assets/encryption"
	"cv_website/assets/views"
	"cv_website/assets/logs"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

func main() {
	// Read the config file
	database.ReadConfig()

	// logs
	logs.LogToFile()

	// generate priv/pub key pair used for cookie
	priv, pub := encryption.GenerateRsaKeys()

	// create a new secret
	encryption.Hs = encryption.NewAuthenticatorInfo("#", priv, pub)

	// start the router handlers and serve those
	err := server()
	if err != nil {
		log.Println(err)
	}
}

// server() : This function connects to the handlers and serves the webserver together with the handlers
func server() error {
	// create the router & port to use
	router := mux.NewRouter()
	port := "443"

	// serve all public files
	staticDirPub := "/public/"
	staticDirPriv := "/private/"
	
	router.PathPrefix(staticDirPub).Handler(http.StripPrefix(staticDirPub, http.FileServer(http.Dir("."+staticDirPub)))).Name("public")
	router.PathPrefix(staticDirPriv).Handler(http.StripPrefix(staticDirPriv, http.FileServer(http.Dir("."+staticDirPriv)))).Name("private")

	// handlers with the GET method
	router.HandleFunc("/", views.Login).Methods("GET").Name("login")
	router.HandleFunc("/login", views.Login).Methods("GET").Name("login")
	router.HandleFunc("/index", views.Index).Methods("GET").Name("index")
	router.HandleFunc("/about-me", views.AboutMe).Methods("GET").Name("about-me")
	router.HandleFunc("/contact", views.Contact).Methods("GET").Name("contact")
	router.HandleFunc("/curriculum-vitae", views.Cv).Methods("GET").Name("cv")
	router.HandleFunc("/diplomas-certificates", views.DipCert).Methods("GET").Name("dip-cert")

	// all skills routes
	router.HandleFunc("/skills", views.Skills).Methods("GET").Name("skills")
	router.HandleFunc("/skills/development", views.SkillsDev).Methods("GET").Name("skills-development")
	router.HandleFunc("/skills/server-management", views.SkillsServerM).Methods("GET").Name("skills-server-management")
	router.HandleFunc("/skills/other", views.SkillsOther).Methods("GET").Name("skills-other")

	// handlers with the POST method
	router.HandleFunc("/submit-login", views.SubmitLogin).Methods("POST").Name("submit-login")
	router.HandleFunc("/submit-mail", views.SubmitMail).Methods("POST").Name("submit-mail")

	// 404 error handler
	router.NotFoundHandler = http.HandlerFunc(views.Error404)

	// middleware used for authentication
	router.Use(views.MiddleWare)

	// Serve all the handlers
	return http.ListenAndServe(":"+port, router)
}
