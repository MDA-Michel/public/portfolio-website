package mail

import (
	"cv_website/assets/database"
	"fmt"
	"log"
	"net/smtp"
)

// Sendmail(): function that sends a mail from the web platform
func SendMail(firstname string, lastname string, mailAddress string, subject string, message string) bool {
	from := database.GetValue("#")
	pass := database.GetValue("#")
	to := database.GetValue("#")

	msg := fmt.Sprintf("From: " + from + "\n" +
			"To: " + to + "\n" +
			"Subject: [WEBMAIL] %s \n\n" + "[SUBJECT]\n%s \n\n [FIRSTNAME]\n%s \n\n [LASTNAME]\n%s \n\n[MAIL]\n%s \n\n\n" + "%s",
			subject, subject, firstname, lastname, mailAddress, message)

	err := smtp.SendMail("#",
		smtp.PlainAuth("", from, pass, "#"),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return false
	}
	return true
}
