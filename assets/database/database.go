package database

import (
	"log"
	"github.com/spf13/viper"
)

// readConfig(): Function is used to read details from the config.yaml file
func ReadConfig() {
	viper.SetConfigName("#")          	   // name of config file (without extension)
	viper.SetConfigType("#")               // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("#") 			   // optionally look for config in the working directory
	err := viper.ReadInConfig()            // Find and read the config file
	if err != nil {                        // Handle errors reading the config file
		log.Println("Fatal error config file: %s \n", err)
		return
	}
}

// GetTokens(): This function is used to get all the tokens needed in able to login
func GetTokens() []string {
	tokens := viper.GetStringSlice("#")
	return tokens
}

// getValue(): returns a value from the config.yaml file
func GetValue(value string) string {
	return viper.GetString(value)
}