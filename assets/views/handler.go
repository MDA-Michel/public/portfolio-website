package views

import (
	"cv_website/assets/database"
	"cv_website/assets/encryption"
	"cv_website/assets/mail"
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"strings"
	"time"
	"github.com/gbrlsnchs/jwt/v3"
)

// getCurrentRoute(): checks which route the user is requesting and returns that
func getCurrentRoute(r *http.Request) string {
	return mux.CurrentRoute(r).GetName()
}

// getAuthorizationCookie(): Returns the bearer key if any was set.
func getAuthorizationCookie(w http.ResponseWriter, r *http.Request) (string, bool) {
    for _, cookie := range r.Cookies() {
        if cookie.Name == "#" {
			cookie.Value = 	strings.Replace(cookie.Value, "#", "#", 1)
            return cookie.Value, true 
        }
    }
	return "", false
}

// MiddleWare(): public function, all private routes will makes use of the middleware to check if they logged in securely
func MiddleWare(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		pl := encryption.CustomPayload{}
		bearerKey := ""
		setToken := false
		routeName := mux.CurrentRoute(r).GetName()

		// list of public routes and private routes
		publicRoutes := []string{"public", "login", "submit-login"};
		privateRoutes := []string{"private", "index", "about-me", "contact", "cv", "skills", "skills-development", "skills-server-management", "skills-other", "submit-mail", "dip-cert"};

		// check if it is a public route, if so continue
		for _, value := range(publicRoutes) {

			if value == routeName {
				if value == "login" {
					bearerKey, setToken = getAuthorizationCookie(w, r)
					_, err := jwt.Verify([]byte(bearerKey), encryption.Hs.Private, &pl, encryption.Authenticator(&pl))
					if err == nil {
						http.Redirect(w, r, "/index", 301)
						break;
					} else {
						next.ServeHTTP(w, r)
						break;
					}
				} else {
					next.ServeHTTP(w, r)
					break;
				}
			}
		}

		// check if it is a private route, if so, start authentication process, if true, contine, if not, redirect
		for _, value := range(privateRoutes) {
			if value == routeName {
				bearerKey, setToken = getAuthorizationCookie(w, r)
				if setToken {					
					_, err := jwt.Verify([]byte(bearerKey), encryption.Hs.Private, &pl, encryption.Authenticator(&pl))
					if err != nil {
						w.Write([]byte("ERROR: Invalid token provided! Please login again at '/' or '/login'"))
						return
					} else {
						// logs which route the user visits
						if value != "private" {
							log.Println(fmt.Sprintf("Company '%s' has visited route '%s'", pl.CompanyName, value))
						}
						next.ServeHTTP(w, r)
					}
				} else {
					http.Redirect(w, r, "/login", 301)
				}
			} 
		}

	})
}

// Error404(): public function, handles the error page once a route pops up that does not exist
func Error404(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/404.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// SubmitMail(): public function. Uses smtp.go to send a mail towards the recipient
func SubmitMail(w http.ResponseWriter, r *http.Request){
	err := r.ParseForm()

	if err != nil {
		w.Write([]byte("Parse error"))
		return
	}

	firstName := r.PostForm.Get("first-name")
	Lastname := r.PostForm.Get("last-name")
	email := r.PostForm.Get("email")
	subject := r.PostForm.Get("subject")
	message := r.PostForm.Get("message")
	send := mail.SendMail(firstName, Lastname, email, subject, message)

	if send {
		http.SetCookie(w, &http.Cookie{Name: "mail-send", Value: "true", HttpOnly: false, Expires: time.Now().Add(5 * time.Second)})
		http.Redirect(w, r, "/contact", 301)
	} else {
		w.Write([]byte("Mail has not been send, please try again! \n If that still does not solve the problem, please send a mail directly to '#'"))
	}
}

// Login(): public function (Handler) Login: Handles the login page
func Login(w http.ResponseWriter, _ *http.Request) {
	index, err := template.ParseFiles("assets/views/html/login.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}

}

// SubmitLogin(): checks if the token entered is a correct token, if so a cookie will be set which will allow the user to access different pages
func SubmitLogin(w http.ResponseWriter, r *http.Request) {
	tokens := database.GetTokens()
	err := r.ParseForm()

	if err != nil {
		w.Write([]byte("Parse error"))
		return
	}

	userToken := r.PostForm.Get("token")
	companyName := r.PostForm.Get("company")

	for index, token := range tokens {
		http.SetCookie(w, &http.Cookie{Name: "visited", Value: "true", HttpOnly: false, Expires: time.Now().Add(1024 * time.Hour)})
		if token == userToken {
			log.Println(fmt.Sprintf("succes! Company %s has logged in", companyName))

			// set a jwt if the user logs in succesfully
			bearerKey, tokenSet := encryption.SetToken(encryption.Response{Token: userToken, CompanyName: companyName}, w)
			if tokenSet {
				stringBearerKey := string(bearerKey)
				http.SetCookie(w, &http.Cookie{Name: "Authorization", Value: "Bearer " + stringBearerKey, HttpOnly: false, Expires: time.Now().Add(2 * time.Hour)})
				http.Redirect(w, r, "/index", 301)
			}
		  	break;
		} else if index == len(tokens)-1 {
			log.Println(fmt.Sprintf("No succes, company '%s' has tryed to log in", companyName))
			http.Redirect(w, r, "/login", 301)
		}
	}
}

// Index(): public function (Handler) Index: Handles the index page
func Index(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/index.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// AboutMe(): public function (Handler) AboutMe: Handles the about-me page
func AboutMe(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/aboutMe.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// Contact(): public function (Handler) Contact: Handles the contact page
func Contact(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/contact.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// Cv(): public function (Handler) Cv: Handles the curriculum-vitae page
func Cv(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/cv.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// DipCert(): public function (handler) DipCert: handles the diplomas-certificates page
func DipCert(w http.ResponseWriter, r *http.Request){
	index, err := template.ParseFiles("assets/views/html/dip-cert.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// Skills(): public function (Handler) Skills: Handles the knowledge-base page
func Skills(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/skills.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// SkillsDev(): public function (Handler) Skills: Handles a sub-page named /skills/development
func SkillsDev(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/skills-development.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml", "assets/views/html/templates/particles.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// SkillsServerM(): public function (Handler) Skills: Handles a sub-page named /skills/server-management
func SkillsServerM(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/skills-server-management.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

// SkillsOther(): public function (Handler) Skills: Handles a sub-page /skills/other
func SkillsOther(w http.ResponseWriter, r *http.Request) {
	index, err := template.ParseFiles("assets/views/html/skills-other.gohtml", "assets/views/html/templates/navbar.gohtml", "assets/views/html/templates/footer.gohtml")
	if err != nil {
		log.Println(err)
		return
	}
	err = index.Execute(w, nil)
	if err != nil {
		log.Println(err)
		return
	}
}

