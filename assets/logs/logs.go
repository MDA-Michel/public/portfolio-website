package logs 

import(
	"cv_website/assets/database"
	"time"
	"log"
	"os"
	"io"
 )

// LogToFile(): logs all logs made within the application to a file
func LogToFile(){
	dt := time.Now()
	logFile := database.GetValue("#") + "-" + dt.Format("01-02-2006") + ".txt"
	// check if exist, if not create file, if so break out of loop
	for {
		if checkLogFile(logFile) == false {
			createEmptyLogFile(logFile)
			log.Println("Created log file: ", logFile)
		} else {
			break;
		}
	}

	// open the file in order to log in the file
	file, e := os.OpenFile(logFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if e != nil {
		log.Println("Could not open the file! ERROR: ", e)
	}
	
	// log everything to a file
	multi := io.MultiWriter(file, os.Stdout)
	log.SetOutput(multi)
}

// checkLogFile(): checks if a file exists, if so returns true, else return false
func checkLogFile(location string) bool {
	_, err := os.Stat(location)
	if err != nil {
		return false
	}
	return true
}

// createEmptyLogFile(): creates an empty .txt file based on what is described in the database/config.yaml file
func createEmptyLogFile(location string){
	os.Create(location)
}