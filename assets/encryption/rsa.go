package encryption

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"cv_website/assets/database"
)

// read public/private keys or generate new ones if we can't find any
func GenerateRsaKeys() (*rsa.PrivateKey, *rsa.PublicKey){
	priv, pub, err := getRsaKeys(database.GetValue("#"))
	if err != nil {
		if strings.Contains(err.Error(), "no such file or directory") {
			priv, err = generateRsaKeys(rand.Reader, 8192)
			if err != nil {
				log.Println(err)
				return &rsa.PrivateKey{}, &rsa.PublicKey{}
			}
			pub = &priv.PublicKey
		} else {
			log.Println(err)
			return &rsa.PrivateKey{}, &rsa.PublicKey{}
		}
	}
	return priv, pub
}

// generate a rsa key pair based of the reader and bit size
// in general the reader should be rand.reader
func generateRsaKeys(reader io.Reader, bitSize int) (*rsa.PrivateKey, error) {
	key, err := rsa.GenerateKey(reader, bitSize)
	if err != nil {
		return nil, errors.New("there was an error while generating the key:" + err.Error())
	}
	err = savePrivateKey(key)
	if err != nil {
		log.Println(err)
	}
	err = savePublicKey(&key.PublicKey)
	if err != nil {
		log.Println(err)
	}
	return key, nil
}


// read the private and public key
func getRsaKeys(location string) (*rsa.PrivateKey, *rsa.PublicKey, error) {
	bytes, err := ioutil.ReadFile(location + "#")
	if err != nil {
		return &rsa.PrivateKey{}, &rsa.PublicKey{}, err
	}
	priv := BytesToPrivateKey(bytes)
	bytes, err = ioutil.ReadFile(location + "#")
	if err != nil {
		return &rsa.PrivateKey{}, &rsa.PublicKey{}, err
	}
	pub := BytesToPublicKey(bytes)
	return priv, pub, nil
}

// convert a private key to an byte array
func PrivateKeyToBytes(priv *rsa.PrivateKey) []byte {
	privateBytes := pem.EncodeToMemory(
		&pem.Block{
			Type:  "#",
			Bytes: x509.MarshalPKCS1PrivateKey(priv),
		},
	)
	return privateBytes
}

// convert public key to a byte array
func PublicKeyToBytes(pub *rsa.PublicKey) []byte {
	pubASN1, err := x509.MarshalPKIXPublicKey(pub)
	if err != nil {
		log.Println(err)
	}
	pubBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "#",
		Bytes: pubASN1,
	})
	return pubBytes
}

// convert a byte array to a private key
func BytesToPrivateKey(priv []byte) *rsa.PrivateKey {
	block, _ := pem.Decode(priv)
	enc := x509.IsEncryptedPEMBlock(block)
	b := block.Bytes
	var err error
	if enc {
		log.Println("is encrypted pem block")
		b, err = x509.DecryptPEMBlock(block, nil)
		if err != nil {
			log.Println(err)
		}
	}
	key, err := x509.ParsePKCS1PrivateKey(b)
	if err != nil {
		log.Println(err)
	}
	return key
}

// convert a byte array to a public key
func BytesToPublicKey(pub []byte) *rsa.PublicKey {
	block, _ := pem.Decode(pub)
	enc := x509.IsEncryptedPEMBlock(block)
	b := block.Bytes
	var err error
	if enc {
		log.Println("is encrypted pem block")
		b, err = x509.DecryptPEMBlock(block, nil)
		if err != nil {
			log.Println(err)
		}
	}
	ifc, err := x509.ParsePKIXPublicKey(b)
	if err != nil {
		log.Println(err)
	}
	key, ok := ifc.(*rsa.PublicKey)
	if !ok {
		log.Println("key not ok")
	}
	return key
}

// save the private key to priv.key
func savePrivateKey(key *rsa.PrivateKey) error {
	_ = os.Remove("#")
	f, err := os.Create(database.GetValue("#") + "#")
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.Write(PrivateKeyToBytes(key))
	return err
}

// save the public key to pub.key
func savePublicKey(key *rsa.PublicKey) error {
	_ = os.Remove("#")
	f, err := os.Create(database.GetValue("#") + "#")
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.Write(PublicKeyToBytes(key))
	return err
}
