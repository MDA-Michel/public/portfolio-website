package encryption

import (
	"crypto/rsa"
	"log"
	"net/http"
	"time"
	"github.com/gbrlsnchs/jwt/v3"
)

// Response: contains a struct which will hold all the details of a specific user
type Response struct {
	Token        string		`json:"token,omitempty"`
	CompanyName  string 	`json:"company,omitempty"`
}

// AuthenticatorInfo: contains global auth info like secret and issuer
type AuthenticatorInfo struct {
	Private *jwt.RSASHA
	Issuer  string
	Aud     jwt.Audience
}

// CustomPayload: contains a struct which is encrypted within the jwt token.
type CustomPayload struct {
	jwt.Payload 		  `json:"payload,omitempty"`
	CompanyName   string  `json:"company,omitempty"`
	Token   	  string  `json:"token,omitempty"`
}

// Hs: variable with type 'AuthenticatorInfo', points to the memory address
var Hs *AuthenticatorInfo

// NewAuthenticatorInfo(): set the issuer, private and public key
func NewAuthenticatorInfo(issuer string, private *rsa.PrivateKey, pub *rsa.PublicKey) *AuthenticatorInfo {
	return &AuthenticatorInfo{Private: jwt.NewRS512(jwt.RSAPublicKey(pub),
		jwt.RSAPrivateKey(private)),
		Issuer: issuer}
}

// SetToken(): returns the token once the username and password have been verified. Used with /login handler
func SetToken(response Response, w http.ResponseWriter) ([]byte, bool) {
	now := time.Now()
	bearerKey, err := jwt.Sign(CustomPayload{Payload: jwt.Payload{
		Issuer:         Hs.Issuer,
		Subject:        "#",
		ExpirationTime: jwt.NumericDate(now.Add(2 * time.Hour)),
		NotBefore:      nil,
		IssuedAt:       jwt.NumericDate(now),
	},
		CompanyName: response.CompanyName,
		Token: response.Token,
	},
		Hs.Private,
	)
	if err != nil {
		log.Println(err)
		w.Write([]byte("Parse error, failed to set token"))
		return []byte(""), false
	}
	return bearerKey, true
}

// Authenticator(): Authenticates a given payload/token
func Authenticator(p *CustomPayload) jwt.VerifyOption {
	t := time.Now()
	return jwt.ValidatePayload(&p.Payload, jwt.IssuedAtValidator(t), jwt.ExpirationTimeValidator(t))
}
