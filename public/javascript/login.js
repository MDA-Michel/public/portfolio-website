// jQuery javascript
$(document).ready(function(){
    // hide form-intro from when the user has visited the website before
    if (getCookie("visited") == "true"){
        checkLoginClicked();
        $("#form-intro").fadeIn(2000)
        $("#form-intro-submit").click(function(e){        
            $(".container").fadeOut(1500);
            $(".bg").toggleClass("zoom");
            e.preventDefault();
            setTimeout(function(){
                $("#form-intro-form").submit(); 
            }, 4000);
        });  
    } else {
        $("#form-intro").hide();
        setTimeout(function(){ 
            $("#form-intro").fadeIn(2000)    
        }, 20000);
        
        $("#form-intro-submit").click(function(e){        
            $(".container").fadeOut(1500);
            $(".bg").toggleClass("zoom");
            e.preventDefault();
            setTimeout(function(){
                $("#form-intro-form").submit(); 
            }, 4000);
        });
    }    
});

// checkLoginClicked(): checks if the login button has been clicked. If so it will show the form & paste the text
function checkLoginClicked(){
    $('#app_1').attr('id','app_2');
    document.getElementById('app_2').innerHTML = 
    `
    <span class=\"title-intro\"> Welcome! </span> 
    <br> 
    <br> 
    I am going to show you a little about me
    <br>
    <br>
    Please login first by entering the code underneath which you've recieved by mail
    `;
    $("#form-intro").fadeIn(2000);
} 

// getCookie(): gets the value of a cookie
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}