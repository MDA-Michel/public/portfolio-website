// used for the login page - intro
var app = document.getElementById('app_1');

var typewriter_1 = new Typewriter(app, {
    delay: 50
});

typewriter_1.typeString('<span class="title-intro">Welcome!</span>')
    .pauseFor(500)
    .typeString("<br>")
    .pauseFor(500)
    .typeString("<br>")
    .pauseFor(1000)
    .typeString("I am going to show you a little about my passion for IT")
    .pauseFor(1500)
    .deleteChars(14)
    .typeString("hobbies")
    .pauseFor(1500)
    .deleteChars(9)
    .typeString("e")
    .pauseFor(1500)
    .typeString("<br>")
    .pauseFor(500)
    .typeString("<br>")
    .pauseFor(500)
    .typeString("Please login first by entering the code underneath which you've recieved by mail")
    .start();
